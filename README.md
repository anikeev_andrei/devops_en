This repository presents a diploma project for the "DevOps for Operations and Development" course from Yandex Practicum.
During the completion of the diploma project, a full cycle of application build-delivery was implemented, using CI/CD practices.
- The source code is stored in this repository.
- Pipelines with a description of the backend (Golang) and frontend (JavaScript) build steps are written.
- Build artifacts (docker images) are published with versioning in GitLab Container Registry..
- Dockerfiles are written for building backend and frontend Docker images.
- A Kubernetes cluster is deployed in Yandex Cloud using Terraform.
- The state of Terraform is stored in S3.
- Non-binary files (images) are stored in S3.
- Helm charts are written for the application's publication.
- The application is deployed in the Kubernetes cluster using ArgoCD.
- Helm charts are published and versioned in Nexus.
- The application is connected to logging and monitoring systems (Prometheus + Grafana).


# PROJECT STRUCTURE

```bash
|
├── backend                       - backend source code, Dockerfile, gitlab-ci.yml
├── frontend                      - frontend source code, Dockerfile, gitlab-ci.yml
├── momo-store-chart              - helm charts for deploying the momo-store application in a Kubernetes cluster
├── terraform                     - Terraform files for deploying a Kubernetes cluster
├── .gitlab-ci.yml                - root pipeline for building backend and frontend images
└── .helm-chart.gitlab-ci.yml     - pipeline for uploading helm charts to the Nexus repository
```
Ready-made backend and frontend images are placed in the GitLab Container Registry.<br>
Helm charts are hosted in the Nexus repository.<br>
The store is deployed in the Kubernetes cluster using ArgoCD.<br>
Prometheus is installed and connected to the backend.<br>
Grafana is installed.<br>


# APPLICATION LAUNCH

## 0 Preparatory actions.

### 0.1 Save the **terraform** directory locally.

### 0.2 Create storage in Yandex.Cloud for storing the terraform state:
*All Services - Object Storage - Create Bucket*

### 0.3 In the file terraform/s3-backend.tf, specify the name of the created storage:
bucket = "<storage_name>"

### 0.4 Create a Service Account with the Editor role:
*All Services - Yandex Cloud Console - Service Accounts - Create service account*

### 0.5 Create environment variables, specifying the Key ID and Secret Key of the created Service Account:
```bash
export ACCESS_KEY="<key_id>"
export SECRET_KEY="<secret_key>"
```

### 0.6 To initialize terraform with state storage in S3, execute the following commands:
```bash
cd terraform
terraform init -backend-config="access_key=$ACCESS_KEY" -backend-config="secret_key=$SECRET_KEY"
```

## 1. Creating a Kubernetes Cluster in Yandex.Cloud

### 1.1
In the file terraform/main.tf in the section for creating storage
for images (yandex_storage_bucket), it is necessary to change the name of the bucket:

bucket = "<bucket_name>"

### 1.2
Execute the commands to create the Kubernetes cluster and infrastructure:
```bash
cd terraform
terraform plan
terraform apply
```

Find out the ID of your Kubernetes cluster:

*Dashboard Catalog - Managed Service for Kubernetes - Clusters - mycluster*

## 2. Setting up the Kubernetes Cluster Configuration /.kube/config

### 2.1 To obtain credentials, execute:
```bash
yc managed-kubernetes cluster get-credentials --id <cluster_id> --external
```
### 2.2 To check the cluster's availability:
```bash
kubectl cluster-info
```
### 2.3 Perform a backup of the current ./kube/config configuration:
```bash
cp ~/.kube/config ~/.kube/config.bak
```
### 2.4 Create the manifest gitlab-admin-service-account.yaml:

```bash
apiVersion: v1
kind: ServiceAccount
metadata:
  name: gitlab-admin
  namespace: kube-system
---
apiVersion: rbac.authorization.k8s.io/v1
kind: ClusterRoleBinding
metadata:
  name: gitlab-admin-role
roleRef:
  apiGroup: rbac.authorization.k8s.io
  kind: ClusterRole
  name: cluster-admin
subjects:
- kind: ServiceAccount
  name: gitlab-admin
  namespace: kube-system
```
and apply it:
```bash
kubectl apply -f gitlab-admin-service-account.yaml
```
### 2.5 Get the endpoint: the public IP address can be found at 
*Managed Service for Kubernetes - Clusters - your_cluster - overview - main - Public IPv4*

### 2.6 Obtain the KUBE_TOKEN:
```bash
kubectl -n kube-system get secrets -o json | jq -r '.items[] | select(.metadata.name | startswith("gitlab-admin")) | .data.token' | base64 --decode
```
### 2.7 Set up the configuration:
```bash
export KUBE_URL=https://<see item 2.5>
export KUBE_TOKEN=<see item 2.6>
export KUBE_USERNAME=gitlab-admin
export KUBE_CLUSTER_NAME=<cluster_id>

kubectl config set-cluster "$KUBE_CLUSTER_NAME" --server="$KUBE_URL" --insecure-skip-tls-verify=true
kubectl config set-credentials "$KUBE_USERNAME" --token="$KUBE_TOKEN"
kubectl config set-context default --cluster="$KUBE_CLUSTER_NAME" --user="$KUBE_USERNAME"
kubectl config use-context default
```

## 3. Setting up access to services from the Internet.

### 3.1 Install the NGINX Ingress Controller with a Let's Encrypt certificate manager:

Installation of NGINX Ingress Controller:

```bash
kubectl apply -f https://raw.githubusercontent.com/kubernetes/ingress-nginx/controller-v1.5.1/deploy/static/provider/cloud/deploy.yaml
```

Installation of the certificate manager:
  
```bash
kubectl apply -f https://github.com/jetstack/cert-manager/releases/download/v1.6.1/cert-manager.yaml
```

### 3.2 Find out the IP address of the Ingress Controller

the value EXTERNAL-IP:
```bash
kubectl get svc -n ingress-nginx
```

### 3.3 On the site https://freedns.afraid.org/subdomain/ create a subdomain

when creating, specify the external IP address of the Ingress Controller.

### 3.4
Specify the created domain in the file **momo-store-chart\values.yaml**:
```bash
frontend:
  fqdn: <>
```
...
```bash
  ingress:
    rules:
      - host: <>
```

### 3.5 Issue a certificate using Certificate Yandex Manager, Let's Encrypt:
https://cloud.yandex.com/en/docs/certificate-manager/operations/managed/cert-create?from=int-console-empty-state

## 4. To enable downloading images from Container Registry to Kubernetes, authorization must be set up::

Create a secret:

```bash
kubectl create secret docker-registry docker-config-secret --docker-server=gitlab.praktikum-xxxx.ru:xxxx   --docker-username=<specify_your_login>   --docker-password=<specify_your_password>
```
Create a service account:

```bash
kubectl create serviceaccount my-serviceaccount
kubectl patch serviceaccount my-serviceaccount -p '{"imagePullSecrets": [{"name": "docker-config-secret"}]}' -n default 
```

## 5. Create a Nexus repository to store helm charts
in Gitlab in the CI/CD settings, add variables:<br>
NEXUS_REPO_USER (login in Nexus)<br>
NEXUS_REPO_PASS (password)<br>
NEXUS_REPO_URL (repository link)<br>

## 6. Changing the image storage location.

### 6.1.
Save images to the created S3 storage (see item 1.1)

### 6.2
Change the paths to image files in the backend files:
\backend\cmd\api\dependencies\store.go
\backend\cmd\api\app\app_test.go

## 7. Installation and Configuration of ArgoCD
https://cloud.yandex.com/en/docs/managed-kubernetes/operations/applications/argo-cd

### 7.1 Install ArgoCD
```bash
helm pull oci://cr.yandex/yc-marketplace/yandex-cloud/argo/chart/argo-cd \
  --version 5.4.3-7 \
  --untar && \
helm install \
  --namespace argocd \
  --create-namespace \
  argo-cd ./argo-cd/
```

### 7.2 Configuration of ArgoCD
#### 7.2.1 To find out the temporary password, execute:

```bash
kubectl -n argocd get secret argocd-initial-admin-secret -o jsonpath="{.data.password}" | base64 -d
```

#### 7.2.2 To access ArgoCD, execute:
```bash
kubectl port-forward service/argo-cd-argocd-server -n argocd 8080:443
```
In the browser, go to http://localhost:8080<br>
login: admin<br>
password: the obtained temporary password<br>

Change the password.

#### 7.2.3 Adding a Kubernetes Cluster
Without switching off the port forwarding, check the current context:
```bash
kubectl config current-context
```
Log in to ArgoCD via CLI:
```bash
argocd login http://localhost:8080
```
Complete the authorization. Add the cluster:
```bash
argocd cluster add [CONTEXT_NAME]
```
Make sure the cluster is added.
```bash
argocd cluster list
```

## 8. Launching the Application.

In the browser, go to ArgoCD at http://localhost:8080

### 8.1 Add a Repository

*Settings - Repositories - Connect repo*

Choose your connection method: VIA HTTPS<br>
Type: helm<br>
Name: *Create a name*<br>
Project: default<br>
Repository URL: *Nexus repository URL*<br>
Username: *Nexus repository login*<br>
Password: *Nexus repository password*<br>

CONNECT

CONNECTION STATUS should be "Successful".

### 8.2 Check Project Settings

*Settings - Projects - default*

SCOPED REPOSITORIES: *Nexus repository URL*<br>
DESTINATIONS:<br>
  Server: *URL of the Kubernetes cluster*<br>
  Namespace: default<br>

### 8.3 Create a New Application

*Application - NEW APP*

Application Name: *Create a name*<br>
Project Name: default<br>
Repository URL: *Nexus repository URL*<br>
Chart: momo-store<br>
Version: *specify the version*<br>
Cluster URL: *URL of the Kubernetes cluster*<br>
Namespace: default<br>

CREATE

After the synchronization is complete, check the availability of the store by its domain address.


Enjoy!
You are breathtaking!
